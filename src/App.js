import React from 'react';
import './App.css';
import { Link } from "react-router-dom"
import Home from "./components/Paginas/Home"
import Aboutme from "./components/Paginas/Aboutme"

import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom"

function App() {
  return (
    <div className="App">
    <Router>
        <Switch>
          <Route
            path="/"
            exact
            render={props => <Home/>}
          />
          <Route
            path="/aboutme"
            exact
            render={props => <Aboutme/>}
          />
          
        </Switch>
      </Router>

    

    </div>
  );
}

export default App;
