import React, {Component} from 'react';
import LayoutMain from '../Layouts/LayoutMain'
import Carrusel from '../Layouts/Carrusel'
import CardJumbo from "../Layouts/CardJumbo"
import Card1 from "../Layouts/Card1"
import PersonList from "../../components/PersonList"
import { Card, CardImg, CardText, CardBody, Row, Col,
    CardTitle, CardSubtitle, Button } from 'reactstrap';
export default class Home extends Component {
    render() {
        return (
            <LayoutMain>
            <CardJumbo/>
            <Carrusel/>
            
            <Card1/>
            <PersonList/>
            </LayoutMain>
        )
    }
}

