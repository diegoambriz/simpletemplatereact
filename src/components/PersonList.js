import React from 'react';

import axios from 'axios';

export default class PersonList extends React.Component {
  state = {
    titles: []
  }

  componentDidMount() {
    axios.get(`http://localhost:3001/api/todo`)
      .then(res => {
        const titles = res.data;
        this.setState({ titles });
      })
  }

  render() {
    return (
      <ul>
        { this.state.titles.map(title => <li>{title.title}</li>)}
      </ul>
    )
  }
}