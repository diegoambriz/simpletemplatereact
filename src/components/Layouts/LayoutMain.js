import React, { Component, Fragment } from 'react'
import Navegation from './Navegation'
import Footer from './Footer'
import CardJumbo from './CardJumbo'

 class LayoutMain extends Component {
    render(){
        return(
            <Fragment>
                <Navegation/>
                <div className="container">
                    {this.props.children}
                </div>
                <Footer/>
            </Fragment>
        )
    }
}
function hello() {
    console.log("Hello")
}
export {hello}
export default LayoutMain