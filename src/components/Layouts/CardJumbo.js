import React from 'react';
import { Jumbotron, Container } from 'reactstrap';

const CardJumbo = (props) => {
  return (
    <div>
      <Jumbotron fluid>
        <Container fluid>
          <h1 className="display-3">Publicaciones</h1>
          <p className="lead">Sucesos relevantes de la Softtekiada 2019</p>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default CardJumbo;
